// Protractor configuration file, see link for more information
// https://github.com/angular/protractor/blob/master/lib/config.ts

const { SpecReporter } = require("jasmine-spec-reporter");
const { DOCKER, FIREFOX, EXTENSION } = process.env;

exports.config = {
  allScriptsTimeout: 21000, // odd number makes it easier to identify
  specs: ["./src/**/*.e2e-spec.ts"],
  capabilities: FIREFOX
    ? {
        browserName: "firefox"
      }
    : {
        browserName: "chrome"
      },
  directConnect: true,
  framework: "jasmine",
  jasmineNodeOpts: {
    showColors: true,
    defaultTimeoutInterval: 41000,
    print: function() {}
  },
  onPrepare() {
    require("ts-node").register({
      project: require("path").join(__dirname, "./tsconfig.e2e.json")
    });
    jasmine
      .getEnv()
      .addReporter(new SpecReporter({ spec: { displayStacktrace: true } }));
  }
};

if (process.env.IS_CI) {
  exports.config.capabilities.chromeOptions = {
    args: ["no-sandbox", "headless", "disable-gpu"]
  };
}
