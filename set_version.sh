#!/bin/sh
PACKAGE_VERSION=$(cat package.json | grep version | head -1 | awk -F= "{ print $2 }" | sed 's/[version:,\",]//g' | tr -d '[[:space:]]')
sed -i -e "s/CI_INSERT_VERSION_HERE/$PACKAGE_VERSION/g" src/manifest.json
