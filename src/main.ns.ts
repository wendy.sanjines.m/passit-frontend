import { platformNativeScriptDynamic } from "nativescript-angular/platform";
import { AppOptions } from "nativescript-angular/platform-common";

import { AppModule } from "./app/app.module";

const options: AppOptions = {};

platformNativeScriptDynamic(options).bootstrapModule(AppModule);
