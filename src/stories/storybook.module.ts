import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { SvgDemoComponent } from "./svg-demo/svg-demo.component";

const COMPONENTS = [
  SvgDemoComponent
];

@NgModule({
  declarations: COMPONENTS,
  imports: [CommonModule],
  exports: COMPONENTS
})
export class StorybookModule {}
