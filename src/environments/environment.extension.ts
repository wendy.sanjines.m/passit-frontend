export const environment = {
  production: true,
  extension: true,
  docker: false,
  nativescript: false,
  VERSION: require("../../package.json").version
};
