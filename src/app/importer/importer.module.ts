import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { StoreModule } from "@ngrx/store";
import { TooltipModule } from "ngx-tooltip";
import { FormsModule } from "@angular/forms";

import { ImporterContainer } from "./importer.container";
import { ImporterComponent } from "./importer.component";
import { ImporterService } from "./importer.service";
import { importerReducer } from "./importer.reducer";
import { InlineSVGModule } from "ng-inline-svg";
import { ProgressIndicatorModule } from "../progress-indicator/progress-indicator.module";
import { ImporterRoutingModule } from "./importer-routing.module";
import { SharedModule } from "../shared/shared.module";

@NgModule({
  imports: [
    CommonModule,
    InlineSVGModule,
    ProgressIndicatorModule,
    FormsModule,
    TooltipModule,
    SharedModule,
    ImporterRoutingModule,
    StoreModule.forFeature("importer", importerReducer)
  ],
  declarations: [ImporterContainer, ImporterComponent],
  providers: [ImporterService]
})
export class ImporterModule {}
