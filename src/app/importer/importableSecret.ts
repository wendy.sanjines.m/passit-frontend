import { INewSecret } from "../../passit_sdk/sdk.interfaces";

export interface ImportableSecret extends INewSecret {
  doImport: boolean;
  importable: boolean;
}
