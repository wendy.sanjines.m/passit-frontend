import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Store, select } from "@ngrx/store";
import { getToken, getUrl, IState } from "../app.reducers";
import { API_URL } from "../constants";

import sdkAPI, { IHeaders } from "../../passit_sdk/api";

@Injectable()
export class Api extends sdkAPI {
  constructor(public http: HttpClient, private store: Store<IState>) {
    super();
    this.store.pipe(select(getToken)).subscribe(token => {
      if (token !== null) {
        this.token = token;
      } else {
        this.token = "";
      }
    });
    // Override base url if set explicitly.
    this.store.pipe(select(getUrl)).subscribe(url => {
      if (url) {
        this.baseUrl = url;
      } else {
        this.baseUrl = API_URL;
      }
    });
  }
  protected async sendInner<B>(
    method: string,
    url: string,
    body: B,
    headers: IHeaders,
    returnsJSON: boolean
  ): Promise<any> {
    const requestOptions: any = {
      body,
      headers: new HttpHeaders(headers),
      observe: "response"
    };
    let resp: any;
    try {
      resp = await this.http.request(method, url, requestOptions).toPromise();
    } catch (res) {
      throw { res };
    }
    if (returnsJSON) {
      return await resp.body;
    }
    return undefined;
  }
}
