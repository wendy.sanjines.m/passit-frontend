import * as api from "../../passit_sdk/api.interfaces";

export interface IGroup extends api.IGroup {
  slug?: string;
}

export interface IContact {
  id: number;
  email: string;
  first_name?: string;
  last_name?: string;
}

/** select2 interface */
export interface ISelectOptions {
  label: string;
  value: any;
}
