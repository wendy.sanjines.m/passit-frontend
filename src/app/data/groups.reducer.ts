import { IGroup } from "./interfaces";
import { EntityState, createEntityAdapter } from "@ngrx/entity";
import { createReducer, Action, on } from "@ngrx/store";
import { setGroups } from "./groups.actions";
import { verifyAndLoginSuccess } from "../account/reset-password/reset-password-verify/reset-password-verify.actions";

export interface IGroupState extends EntityState<IGroup> {}

function sortByName(a: IGroup, b: IGroup) {
  return a.name.localeCompare(b.name);
}
export const adapter = createEntityAdapter<IGroup>({
  sortComparer: sortByName
});

export const initialState: IGroupState = adapter.getInitialState({});

const groupReducer = createReducer(
  initialState,
  on(setGroups, verifyAndLoginSuccess, (state, { groups }) => {
    return adapter.addAll(groups, state);
  })
);

export function reducer(state: IGroupState | undefined, action: Action) {
  return groupReducer(state, action);
}

const { selectAll } = adapter.getSelectors();

export const selectAllGroups = selectAll;
