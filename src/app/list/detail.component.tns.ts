import { Component } from "@angular/core";

/** This is only used in phone layouts
 * It doesn't follow the general smart/dumb comoponent split because
 * there is no web version of this - so we just combine it.
 */
@Component({
  selector: "secret-detail-component",
  moduleId: module.id,
  templateUrl: "./detail.component.html",
})
export class SecretDetailComponent {

  constructor(
  ) {}
}
