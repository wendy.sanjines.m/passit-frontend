import {
  Component,
  EventEmitter,
  Input,
  Output,
  ViewChild,
  ElementRef
} from "@angular/core";
import { FormGroupState, MarkAsSubmittedAction } from "ngrx-forms";
import { ActionsSubject } from "@ngrx/store";
import { ILoginForm } from "./interfaces";

@Component({
  selector: "login-component",
  templateUrl: "./login.component.html",
  styleUrls: ["../shared/account-frame/account-frame.component.css"]
})
export class LoginComponent {
  @Input() form: FormGroupState<ILoginForm>;
  @Input() errorMessage: string;
  @Input() hasLoginStarted: boolean;
  @Input() hasLoginFinished: boolean;
  @Output() login = new EventEmitter<ILoginForm>();
  @Output() goToRegister = new EventEmitter();
  @Output() goToResetPassword = new EventEmitter();

  @ViewChild("password", { static: false }) password: ElementRef;

  constructor(private actionsSubject: ActionsSubject) {}

  onSubmit() {
    if (this.form.isValid) {
      this.login.emit();
    } else if (this.form.isUnsubmitted) {
      this.actionsSubject.next(new MarkAsSubmittedAction(this.form.id));
    }
  }
}
