import { VerifyMFAEffects } from "./verify-mfa/verify-mfa.effects";
import { LoginEffects } from "./login.effects";

export const effects = [LoginEffects, VerifyMFAEffects];
