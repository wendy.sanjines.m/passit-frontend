import { Component, ChangeDetectionStrategy } from "@angular/core";
import { Store, select } from "@ngrx/store";
import {
  IState,
  selectVerifyMfaForm,
  getVerifyMfaHasStarted,
  getVerifyMfaFinished,
  getVerifyMfaErrorMessage
} from "../login.reducer";
import { VerifyMfa } from "./verify-mfa.actions";
import * as fromRoot from "../../app.reducers";
import { IS_EXTENSION } from "../../constants";
import { Router } from "@angular/router";
import { LogoutAction } from "../../account/account.actions";
import { ResetFormContainer } from "../../form";

@Component({
  template: `
    <app-verify-mfa
      [form]="form$ | async"
      [form]="form$ | async"
      [isExtension]="isExtension"
      [hasStarted]="hasStarted$ | async"
      [hasFinished]="hasFinished$ | async"
      [errorMessage]="errorMessage$ | async"
      [isPopup]="isPopup"
      (goToLogin)="goToLogin()"
      (onSubmit)="onSubmit()"
    ></app-verify-mfa>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class VerifyMfaContainer extends ResetFormContainer {
  form$ = this.store.pipe(select(selectVerifyMfaForm));
  hasStarted$ = this.store.pipe(select(getVerifyMfaHasStarted));
  hasFinished$ = this.store.pipe(select(getVerifyMfaFinished));
  errorMessage$ = this.store.pipe(select(getVerifyMfaErrorMessage));

  isExtension = IS_EXTENSION;
  isPopup = false;

  constructor(private router: Router, public store: Store<IState>) {
    super(store);
    store
      .pipe(select(fromRoot.getIsPopup))
      .subscribe(isPopup => (this.isPopup = isPopup));
  }

  goToLogin() {
    // Technically, the user is logged in during this stage. Log them out before redirecting.
    this.store.dispatch(new LogoutAction());
    if (this.isPopup) {
      browser.tabs.create({
        url: "/index.html#/account/login"
      });
      window.close();
    } else {
      this.router.navigate(["/account/login"]);
    }
  }

  onSubmit() {
    this.store.dispatch(new VerifyMfa());
  }
}
