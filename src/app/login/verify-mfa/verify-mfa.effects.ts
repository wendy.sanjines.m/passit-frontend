import { Injectable } from "@angular/core";
import { Effect, Actions, ofType } from "@ngrx/effects";
import {
  VerifyMfaActionTypes,
  VerifyMfaSuccess,
  VerifyMfaFailure
} from "./verify-mfa.actions";
import {
  withLatestFrom,
  map,
  exhaustMap,
  catchError,
  tap
} from "rxjs/operators";
import { Store, select } from "@ngrx/store";
import { IState, selectVerifyMfaForm } from "../login.reducer";
import { UserService } from "../../account/user";
import { of } from "rxjs";
import { Router } from "@angular/router";
import { IS_EXTENSION } from "../../constants";

@Injectable()
export class VerifyMFAEffects {
  @Effect()
  verifyMfa$ = this.actions$.pipe(
    ofType(VerifyMfaActionTypes.VERIFY_MFA),
    withLatestFrom(this.store.pipe(select(selectVerifyMfaForm))),
    map(([action, form]) => form),
    exhaustMap(form => {
      if (form.isValid) {
        return this.service.verifyMfa(form.value.otp).pipe(
          map(resp => new VerifyMfaSuccess()),
          catchError(err => of(new VerifyMfaFailure()))
        );
      }
      return of(new VerifyMfaFailure());
    })
  );

  @Effect({ dispatch: false })
  verifyMfaSuccess$ = this.actions$.pipe(
    ofType(VerifyMfaActionTypes.VERIFY_MFA_SUCCESS),
    tap(() => {
      if (IS_EXTENSION) {
        this.router.navigate(["/popup"]);
      } else {
        this.router.navigate(["/"]);
      }
    })
  );

  constructor(
    private actions$: Actions,
    private store: Store<IState>,
    private service: UserService,
    private router: Router
  ) {}
}
