import * as fromLogin from "./login.reducer";
import { LoginFailureAction } from "../app.actions";

describe("LoginReducer", () => {
  describe("LOGIN_FAILURE", () => {
    it("should return error message", () => {
      const error = "some error";
      const createAction = new LoginFailureAction(error);
      const result = fromLogin.loginFormReducer(
        fromLogin.initialLoginFormState,
        createAction
      );
      expect(result.errorMessage).toBe(error);
      expect(result.hasFinished).toBe(false);
      expect(result.hasStarted).toBe(false);
    });
  });
});
