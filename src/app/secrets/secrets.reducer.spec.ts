import { fakeAsync, inject, TestBed } from "@angular/core/testing";
import { Store, StoreModule, select } from "@ngrx/store";
import { ISecret } from "../../passit_sdk/api.interfaces";
import * as fromApp from "../app.reducers";
import * as SecretActions from "./secret.actions";
import * as fromSecret from "./secrets.reducer";

describe("SecretReducer", () => {
  describe("undefined action", () => {
    it("should return the default state", () => {
      const action = {} as any;
      const result = fromSecret.secretReducer(undefined, action);

      expect(result).toEqual(fromSecret.initialState);
    });
  });

  describe("REMOVE_SECRET", () => {
    it("should remove a secret", () => {
      const removedSecret: ISecret = {
        name: "test",
        type: "test",
        id: 1,
        data: {},
        secret_through_set: []
      };

      const createAction = new SecretActions.RemoveSecretAction(
        removedSecret.id
      );
      const result = fromSecret.secretReducer(
        fromSecret.initialState,
        createAction
      );

      expect(result).not.toContain(removedSecret);
    });
  });

  describe("getSecrets", () => {
    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [StoreModule.forRoot(fromApp.reducers)],
        providers: []
      });
    });

    it("should sort secrets by name", fakeAsync(
      inject([Store], (store: Store<any>) => {
        const getSortedGroupsSelector = store.pipe(select(fromApp.getSecrets));

        const initialState: ISecret[] = [
          {
            id: 1,
            name: "second",
            type: "test",
            data: {},
            secret_through_set: []
          },
          {
            id: 2,
            name: "first",
            type: "test",
            data: {},
            secret_through_set: []
          }
        ];

        store.dispatch(new SecretActions.SetSecretsAction(initialState));

        getSortedGroupsSelector.subscribe(secrets =>
          expect(secrets[0].name).toEqual("first")
        );
      })
    ));
  });
});
