import { TestBed } from "@angular/core/testing";
import { RouterTestingModule } from "@angular/router/testing";
import { provideMockActions } from "@ngrx/effects/testing";
import { cold, hot } from "jasmine-marbles";
import { Observable } from "rxjs";

import { GetSecretsAction, SetSecretsAction } from "./secret.actions";
import { SecretEffects } from "./secret.effects";
import { SecretService } from "./secret.service";
import { testSecret } from "./test_data";

describe("Secret Effects", () => {
  let effects: SecretEffects;
  let secretService: any;
  let actions: Observable<any>;
  const secrets = [testSecret];

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule],
      providers: [
        SecretEffects,
        provideMockActions(() => actions),
        {
          provide: SecretService,
          useValue: jasmine.createSpyObj("SecretService", ["getSecrets"])
        }
      ]
    });

    effects = TestBed.get(SecretEffects);
    secretService = TestBed.get(SecretService);
  });

  it("should get and set secrets", () => {
    actions = hot("a", { a: new GetSecretsAction() });
    const response = cold("-a", { a: secrets });
    secretService.getSecrets.and.returnValue(response);
    const expected = cold("-b", { b: new SetSecretsAction(secrets) });
    expect(effects.getSecrets$).toBeObservable(expected);
  });

  it("should continue working after api error", () => {
    actions = hot("a--b", {
      a: new GetSecretsAction(),
      b: new GetSecretsAction()
    });
    // First error
    const response1 = cold("-#");
    // Then succeed (maybe the user logged in again)
    const response2 = cold("-a", { a: secrets });
    secretService.getSecrets.and.returnValues(response1, response2);
    const expected = cold("----b", {
      b: new SetSecretsAction(secrets)
    });
    expect(effects.getSecrets$).toBeObservable(expected);
  });
});
