import { Injectable } from "@angular/core";
import { knownFolders } from "tns-core-modules/file-system";
import * as application from "tns-core-modules/application";
import { NgPassitSDK } from "../ngsdk/sdk";
import { ExporterServiceBase } from "./exporter.service.common";

@Injectable()
export class ExporterService extends ExporterServiceBase {
  HEADERS = ["name", "username", "url", "password", "extra"];

  constructor(sdk: NgPassitSDK) {
    super(sdk);
  }

  async exportSecrets(): Promise<void> {
    const secrets = await this.getSecrets();
    const csv = this.serialize(secrets);
    const tmpFolder = knownFolders.temp();
    const tmpFileName = "export.csv";
    const tmpFile = tmpFolder.getFile(tmpFileName);
    tmpFile.writeText(csv).then(() => {
      this.shareAndroid({ path: tmpFile.path });
      tmpFile.remove();
    });
  }

  public shareAndroid(args: any) {
    let intent = new android.content.Intent();
    intent.addFlags(android.content.Intent.FLAG_GRANT_READ_URI_PERMISSION);

    let uris = new java.util.ArrayList();
    uris.add(args.path);
    let builder = new android.os.StrictMode.VmPolicy.Builder();
    android.os.StrictMode.setVmPolicy(builder.build());

    intent.setAction(android.content.Intent.ACTION_SEND);
    intent.setType("text/csv");
    intent.putExtra(
      android.content.Intent.EXTRA_STREAM,
      android.net.Uri.parse("file://" + args.path)
    );

    application.android.context.startActivity(
      android.content.Intent.createChooser(
        intent,
        args.intentTitle ? args.intentTitle : "Share With:"
      )
    );
  }
}
