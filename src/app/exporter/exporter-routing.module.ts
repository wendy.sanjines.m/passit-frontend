import { NgModule } from "@angular/core";
import { RouterModule } from "@angular/router";
import { routes } from "./exporter.common";


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ExporterRoutingModule {}
