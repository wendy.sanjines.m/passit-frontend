import { Routes } from '@angular/router';
import { LoggedInGuard } from '../guards';
import { ExporterComponent } from './exporter.component';

export const componentDeclarations: any[] = [
];

export const providerDeclarations: any[] = [
];

export const routes: Routes = [
  {
    path: "",
    canActivate: [LoggedInGuard],
    component: ExporterComponent
  }
];
