require("nativescript-localstorage");
require("nativescript-orientation");

import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptModule } from "nativescript-angular/nativescript.module";
import { StoreModule } from "@ngrx/store";
import { EffectsModule } from "@ngrx/effects";
import { HttpClientModule, HTTP_INTERCEPTORS } from "@angular/common/http";

import { AppRoutingModule } from "./app-routing.module.tns";
import { AppComponent } from "./app.component";
import { AccountModule } from "./account/account.module";
import { reducers, metaReducers } from "./app.reducers";
import { AlreadyLoggedInGuard, LoggedInGuard } from "./guards";
import { ListModule } from "./list";
import { GetConfEffects } from "./get-conf/conf.effects";
import { GetConfService } from "./get-conf/";
import { Api } from "./ngsdk/api";
import { NgPassitSDK } from "./ngsdk/sdk";
import { SecretService } from "./secrets/secret.service";
import { SecretEffects } from "./secrets/secret.effects";
import { GeneratorService } from "./secrets";
import { AppDataService } from "./shared/app-data/app-data.service";
import { MobileMenuModule } from "./mobile-menu";
import { LoginModule } from "./login/login.module";
import { AppEffects } from "./app.effects";
import { AuthInterceptor } from "./api/auth.interceptor";
import { GroupEffects } from "./data/groups.effects";
import { GroupService } from "./groups/group.service";

@NgModule({
  declarations: [AppComponent],
  imports: [
    NativeScriptModule,
    AppRoutingModule,
    AccountModule,
    LoginModule,
    ListModule,
    MobileMenuModule,
    HttpClientModule,
    StoreModule.forRoot(reducers, { metaReducers }),
    EffectsModule.forRoot([
      AppEffects,
      SecretEffects,
      GetConfEffects,
      GroupEffects
    ])
  ],
  providers: [
    AlreadyLoggedInGuard,
    LoggedInGuard,
    GetConfService,
    GroupService,
    { provide: AppDataService, useClass: AppDataService },
    { provide: Api, useClass: Api },
    { provide: NgPassitSDK, useClass: NgPassitSDK },
    { provide: SecretService, useClass: SecretService },
    { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true },
    GeneratorService
  ],
  bootstrap: [AppComponent],
  schemas: [NO_ERRORS_SCHEMA]
})
export class AppModule {}
