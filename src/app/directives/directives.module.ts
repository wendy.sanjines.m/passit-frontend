import { NgModule } from "@angular/core";
import { NativeScriptCommonModule } from "nativescript-angular/common";

import {
  NgrxTextFieldViewAdapter,
  NgrxTextViewViewAdapter
} from "./input.directive";
import { NgrxSwitchViewAdapter } from "./switch.directive";

const COMPONENTS = [
  NgrxTextFieldViewAdapter,
  NgrxTextViewViewAdapter,
  NgrxSwitchViewAdapter
];

@NgModule({
  imports: [NativeScriptCommonModule],
  declarations: COMPONENTS,
  exports: COMPONENTS
})
export class DirectivesModule {}
