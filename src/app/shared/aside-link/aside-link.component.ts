import { Component, Input } from "@angular/core";

/**
 * The aside link component often shows up near form labels, as an alternative option to the form.
 * For example instead of logging, offer to go to forgot password.
 */
@Component({
  selector: "aside-link",
  templateUrl: "./aside-link.component.html",
  styleUrls: ["./aside-link.component.css"]
})
export class AsideLinkComponent {
  @Input() link: string[];
  @Input() tabindex: string;

  constructor() {}
}
