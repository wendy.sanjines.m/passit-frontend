import { Component, ChangeDetectionStrategy, Input } from "@angular/core";

@Component({
  selector: "app-form-label",
  template: `
    <ng-template #innerTemplate>
      <span
        class="form-label__text"
        [class.form-label__text--completed]="isComplete"
        [class.form-label__text--inactive]="isInactive"
      >
        <ng-content></ng-content>
      </span>
      <span
        class="form-label__icon"
        [class.form-label__icon--active]="isComplete"
        [class.form-label__icon--inactive]="isInactive"
        [inlineSVG]="'../assets/svg/check.svg'"
      ></span>
    </ng-template>

    <label
      *ngIf="for"
      [for]="for"
      class="form-label"
      [class.form-label--large]="isLarge"
    >
      <ng-container *ngTemplateOutlet="innerTemplate"></ng-container>
    </label>
    <div
      *ngIf="!for"
      class="form-label"
      [class.form-label--large]="isLarge"
    >
      <ng-container *ngTemplateOutlet="innerTemplate"></ng-container>
    </div>
  `,
  styleUrls: ["./form-label.styles.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class FormLabelComponent {
  @Input()
  isComplete = false;

  @Input()
  isInactive = false;

  @Input()
  isLarge = false;

  @Input()
  for: string;

  constructor() {}
}
