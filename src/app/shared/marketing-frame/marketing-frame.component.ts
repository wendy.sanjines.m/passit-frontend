import { Component, ChangeDetectionStrategy, Input } from "@angular/core";

@Component({
  selector: "app-marketing-frame",
  templateUrl: "./marketing-frame.component.html",
  styleUrls: ["../../account/account.component.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MarketingFrameComponent {
  DEFAULT_HEADLINE = "Gain peace of mind for your passwords";
  DEFAULT_TAGLINE =
    "Store your passwords. Make them safer. Access them anywhere. Share them with others. It all starts here.";

  @Input()
  isPopup: boolean;

  @Input()
  headline: string = this.DEFAULT_HEADLINE;

  @Input()
  tagline: string = this.DEFAULT_TAGLINE;

  constructor() {}
}
