import {
  Component,
  EventEmitter,
  Input,
  Output,
  ChangeDetectionStrategy
} from "@angular/core";

export interface IMultiselectList {
  name: string;
  value: number;
  isSelected?: boolean;
  isPending?: boolean;
  /** Show a warning before allowing user to remove this entry */
  confirmRemoval?: boolean;
}

@Component({
  selector: "app-multiselect-list",
  templateUrl: "./multiselect-list.component.html",
  styleUrls: ["./multiselect-list.component.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MultiselectListComponent {
  @Input() listItems: IMultiselectList[];
  @Input() singleItemIsRemovable: boolean;
  @Input() confirmRemovalMessage: string;
  @Output() removeListItem: EventEmitter<number> = new EventEmitter();

  onRemoveListItem(listItem: IMultiselectList) {
    if (listItem.confirmRemoval) {
      if (window.confirm(this.confirmRemovalMessage)) {
        this.removeListItem.emit(listItem.value);
      }
    } else {
      this.removeListItem.emit(listItem.value);
    }
  }
}
