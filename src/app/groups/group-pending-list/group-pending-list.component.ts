import {
  Component,
  ChangeDetectionStrategy,
  EventEmitter,
  Input,
  Output
} from "@angular/core";
import { IGroup } from "../../data/interfaces";

@Component({
  selector: "app-group-pending-list",
  templateUrl: "./group-pending-list.component.html",
  styleUrls: [
    "./group-pending-list.component.scss",
    "../../list/list.component.scss",
    "../../list/secret-row/secret-row.component.scss"
  ],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class GroupPendingListComponent {
  @Input() pendingInviteGroups: IGroup[];
  @Output() acceptGroup = new EventEmitter<IGroup>();
  @Output() declineGroup = new EventEmitter<IGroup>();
}
