import { EntityState, createEntityAdapter } from "@ngrx/entity";
import { createReducer, Action, on } from "@ngrx/store";
import { IContact } from "../../data/interfaces";
import { getContactsSuccess } from "./contacts.actions";
import { contactLookupSuccess } from "../groups-form/groups-form.actions";

export interface IContactsState extends EntityState<IContact> {}

const adapter = createEntityAdapter<IContact>();

export const initialState: IContactsState = adapter.getInitialState();

const contactsReducer = createReducer(
  initialState,
  on(getContactsSuccess, (state, action) => {
    return adapter.addAll(action.contacts, state);
  }),
  on(contactLookupSuccess, (state, action) => {
    return adapter.addOne(
      {
        email: action.label,
        id: action.value
      },
      state
    );
  })
);

export function reducer(state: IContactsState | undefined, action: Action) {
  return contactsReducer(state, action);
}

const { selectAll } = adapter.getSelectors();

export const selectAllContacts = selectAll;
