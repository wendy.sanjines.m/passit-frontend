import { createAction, props } from "@ngrx/store";
import { IContact } from "../../data/interfaces";

export const getContacts = createAction("[Groups Module] Get Contacts");
export const getContactsSuccess = createAction(
  "[Groups Module] Get Contacts Success",
  props<{ contacts: IContact[] }>()
);
export const getContactsFailure = createAction(
  "[Groups Module] Get Contacts Failure"
);
