import { NgModule, InjectionToken } from "@angular/core";
import { CommonModule } from "@angular/common";
import { InlineSVGModule } from "ng-inline-svg";
import { StoreModule, ActionReducerMap } from "@ngrx/store";
import { NgrxFormsModule } from "ngrx-forms";
import { EffectsModule } from "@ngrx/effects";

import { GroupsRoutingModule } from "./groups-routing.module";
import { GroupsContainer } from "./groups.container";
import { GroupAddButtonComponent } from "./group-add/group-add-button.component";
import { GroupAddComponent } from "./group-add/group-add.component";
import { GroupEmptyStateComponent } from "./group-empty-state/group-empty-state.component";
import { reducers } from "./groups.reducer";
import { GroupListComponent } from "./group-list.component";
import { GroupDetailComponent } from "./group-detail.component";
import { GroupsFormComponent } from "./groups-form/groups-form.component";
import { GroupsFormContainer } from "./groups-form/groups-form.container";
import { SharedModule } from "../shared/shared.module";
import { GroupsFormEffects } from "./groups-form/group-form.effects";
import { ContactsService } from "./contacts/contacts.service";
import { GroupPendingListComponent } from "./group-pending-list/group-pending-list.component";
import { GroupsModuleEffects } from "./groups.effects";

export const REDUCER_TOKEN = new InjectionToken<ActionReducerMap<any>>(
  "groupsModule"
);

@NgModule({
  declarations: [
    GroupsContainer,
    GroupAddButtonComponent,
    GroupAddComponent,
    GroupEmptyStateComponent,
    GroupListComponent,
    GroupDetailComponent,
    GroupsFormComponent,
    GroupsFormContainer,
    GroupPendingListComponent
  ],
  imports: [
    CommonModule,
    GroupsRoutingModule,
    NgrxFormsModule,
    SharedModule,
    StoreModule.forFeature("groupsModule", REDUCER_TOKEN),
    EffectsModule.forFeature([GroupsFormEffects, GroupsModuleEffects]),
    InlineSVGModule
  ],
  providers: [
    {
      provide: REDUCER_TOKEN,
      useValue: reducers
    },
    ContactsService
  ]
})
export class GroupsModule {}
