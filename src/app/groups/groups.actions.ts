import { createAction, props } from "@ngrx/store";
import { IGroup } from "../data/interfaces";

export const showCreate = createAction("[Groups Module] Show Create");
export const hideCreate = createAction("[Groups Module] Hide Create");
export const setManaged = createAction(
  "[Groups Module] Set Managed Group",
  props<IGroup>()
);
export const initGroups = createAction("[Groups Module] Init");
export const acceptInvite = createAction(
  "[Groups Module] Accept Invite",
  props<{ group: IGroup }>()
);
export const declineInvite = createAction(
  "[Groups Module] Reject Invite",
  props<{ group: IGroup }>()
);
