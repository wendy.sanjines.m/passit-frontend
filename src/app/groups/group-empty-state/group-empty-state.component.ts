import { ChangeDetectionStrategy, Component, Input } from "@angular/core";

@Component({
  selector: "app-group-empty-state",
  templateUrl: "./group-empty-state.component.html",
  styleUrls: ["./group-empty-state.component.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class GroupEmptyStateComponent {
  @Input() showCreate: boolean;
  @Input() groupsCount: number;
  @Input() pendingGroupsCount: number;
}
