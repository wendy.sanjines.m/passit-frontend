import { Injectable } from "@angular/core";
import { createEffect, Actions, ofType } from "@ngrx/effects";
import { Store, select } from "@ngrx/store";
import {
  withLatestFrom,
  map,
  mergeMap,
  catchError,
  filter,
  distinct,
  switchMap,
  debounceTime,
  exhaustMap
} from "rxjs/operators";
import { of } from "rxjs";
import {
  saveGroup,
  saveGroupSuccess,
  saveGroupFailure,
  deleteGroup,
  deleteGroupSuccess,
  deleteGroupFailure,
  contactLookupSuccess,
  contactLookupFailure
} from "./groups-form.actions";
import {
  IAppState,
  selectGroupsForm,
  selectGroupsPageGroupManaged,
  selectGroupContacts
} from "../groups.reducer";
import { GroupService } from "../group.service";
import { ContactsService } from "../contacts/contacts.service";
import { getIsPrivateOrgMode, getEmail, getUserId } from "../../app.reducers";
import {
  getContacts,
  getContactsSuccess,
  getContactsFailure
} from "../contacts/contacts.actions";
import { loadGroups } from "../../data/groups.actions";
import { initGroups } from "../groups.actions";

@Injectable()
export class GroupsFormEffects {
  saveGroup$ = createEffect(() =>
    this.actions$.pipe(
      ofType(saveGroup),
      withLatestFrom(
        this.store.pipe(select(selectGroupsForm)),
        this.store.pipe(select(selectGroupsPageGroupManaged)),
        this.store.pipe(select(selectGroupContacts)),
        this.store.pipe(select(getEmail)),
        this.store.pipe(select(getUserId))
      ),
      mergeMap(([_, form, groupManaged, contacts, ownEmail, ownUserId]) => {
        const selectedContacts = contacts.filter(contact => contact.isSelected);
        if (groupManaged === null) {
          // Creating New
          if (
            ownEmail &&
            ownUserId &&
            !selectedContacts.find(contact => contact.id === ownUserId)
          ) {
            selectedContacts.push({
              id: ownUserId,
              email: ownEmail,
              isSelected: true,
              isPending: false
            });
          }
          return this.groupService
            .create(form.value.name, [], selectedContacts)
            .pipe(
              map(() => saveGroupSuccess()),
              catchError(err => of(saveGroupFailure()))
            );
        } else {
          return this.groupService
            .update(groupManaged, form.value.name, "")
            .pipe(
              exhaustMap(() => {
                return this.groupService
                  .updateGroupMembers(groupManaged, selectedContacts)
                  .pipe(
                    map(() => saveGroupSuccess()),
                    catchError(() => of(saveGroupFailure()))
                  );
              }),
              catchError(err => of(saveGroupFailure()))
            );
        }
      })
    )
  );

  deleteGroup$ = createEffect(() =>
    this.actions$.pipe(
      ofType(deleteGroup),
      withLatestFrom(this.store.pipe(select(selectGroupsPageGroupManaged))),
      mergeMap(([_, groupManaged]) => {
        if (groupManaged) {
          return this.groupService
            .deleteGroup(groupManaged)
            .pipe(
              map(
                () => deleteGroupSuccess(),
                catchError(() => of(deleteGroupFailure()))
              )
            );
        } else {
          return of(deleteGroupFailure());
        }
      })
    )
  );

  contactLookup$ = createEffect(() =>
    this.store.pipe(select(selectGroupsForm)).pipe(
      filter(fs => !!fs.value.searchUsers && fs.controls.searchUsers.isValid),
      distinct(fs => fs.value),
      withLatestFrom(this.store.pipe(select(getIsPrivateOrgMode))),
      filter(([_, isPrivateOrgMode]) => !isPrivateOrgMode),
      debounceTime(400),
      switchMap(([form, _]) =>
        this.contactsService.contactLookup(form.value.searchUsers).pipe(
          map(resp =>
            contactLookupSuccess({
              label: form.value.searchUsers,
              value: resp,
              disabled: false
            })
          ),
          catchError(() => [contactLookupFailure()])
        )
      )
    )
  );

  getContacts$ = createEffect(() =>
    this.actions$.pipe(
      ofType(getContacts, loadGroups, saveGroupSuccess, initGroups),
      mergeMap(() =>
        this.contactsService.getContacts().pipe(
          map(contacts => getContactsSuccess({ contacts })),
          catchError(() => of(getContactsFailure()))
        )
      )
    )
  );

  constructor(
    private actions$: Actions,
    private store: Store<IAppState>,
    private groupService: GroupService,
    private contactsService: ContactsService
  ) {}
}
