import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule } from "nativescript-angular/common";

import { GroupsRoutingModule } from "./groups-routing.module";
import { GroupsContainer } from "./groups.container";
import { GroupsFormComponent } from "./groups-form/groups-form.component";
import { GroupPendingListComponent } from "./group-pending-list/group-pending-list.component";
import { GroupListComponent } from "./group-list.component";
import { GroupAddButtonComponent } from "./group-add/group-add-button.component";
import { GroupEmptyStateComponent } from "./group-empty-state/group-empty-state.component";
import { GroupAddComponent } from "./group-add/group-add.component";
import { GroupsFormContainer } from "./groups-form/groups-form.container";

@NgModule({
  declarations: [
    GroupsContainer,
    GroupsFormComponent,
    GroupAddComponent,
    GroupAddButtonComponent,
    GroupListComponent,
    GroupPendingListComponent,
    GroupsFormContainer,
    GroupEmptyStateComponent
  ],
  imports: [GroupsRoutingModule, NativeScriptCommonModule],
  schemas: [NO_ERRORS_SCHEMA]
})
export class GroupsModule {}
