// /*
// * @angular
// */
// import { ComponentFixture, TestBed } from "@angular/core/testing";
//
// /*
// * testing stub files
// */
// import { RouterLinkStubDirective } from "../../testing/router-stubs";
// import { RouterOutletStubDirective } from "../../testing/router-outlet";
//
// /*
// * Passit
// */
// import { AppComponent } from "./app.component";
// import { NavbarComponent } from "./shared/navbar/navbar.component";
//
// /*
// * test setup
// */
// let comp: AppComponent;
// let fixture: ComponentFixture<AppComponent>;
//
// describe("Testing App Component: ", () => {
//
//   beforeEach(() => {
//
//     TestBed.configureTestingModule({
//       declarations: [
//         AppComponent,
//         NavbarComponent,
//         RouterLinkStubDirective,
//         RouterOutletStubDirective
//       ]
//     });
//
//     // create component and test fixture
//     fixture = TestBed.createComponent(AppComponent);
//
//     // AppComponent test instance
//     comp = fixture.componentInstance;
//   });
//
//   it("should display 'Passit' title", () => {
//     expect(comp.title).toEqual("Passit");
//   });
//
// });
