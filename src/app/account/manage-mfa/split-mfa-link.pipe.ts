import { Pipe, PipeTransform } from "@angular/core";

@Pipe({
  name: "splitMfaLink"
})
export class SplitMfaLinkPipe implements PipeTransform {
  transform(uri: string, args?: any): string {
    const splitUriBefore = uri.split("secret=", 2);
    const splitUriAfter = splitUriBefore[1].split("&issuer", 2);
    return splitUriAfter[0];
  }
}
