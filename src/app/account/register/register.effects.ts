import { map, exhaustMap, withLatestFrom, catchError } from "rxjs/operators";
import { of } from "rxjs";
import { Injectable } from "@angular/core";
import { Actions, createEffect, ofType } from "@ngrx/effects";
import { Store, select } from "@ngrx/store";

import * as fromAccount from "../account.reducer";
import {
  checkEmailSuccess,
  setIsEmailTaken,
  checkEmail,
  checkEmailFailure,
  checkUrl,
  checkUrlSuccess,
  checkUrlFailure,
  register,
  registerSuccess,
  registerFailure
} from "./register.actions";
import { UserService } from "../user";
import { IState } from "../../app.reducers";

@Injectable()
export class RegisterEffects {
  checkEmail$ = createEffect(() =>
    this.actions$.pipe(
      ofType(checkEmail),
      withLatestFrom(this.store.pipe(select(fromAccount.getRegisterForm))),
      map(([action, form]) => form),
      exhaustMap(form => {
        return this.userService.checkUsername(form.value.email).pipe(
          map(resp => {
            if (resp.isAvailable) {
              return checkEmailSuccess();
            } else {
              return setIsEmailTaken({ payload: true });
            }
          }),
          catchError(err => of(checkEmailFailure(err)))
        );
      })
    )
  );

  checkUrl$ = createEffect(() =>
    this.actions$.pipe(
      ofType(checkUrl),
      withLatestFrom(this.store.pipe(select(fromAccount.getUrlForm))),
      map(([action, form]) => form),
      exhaustMap(form => {
        return this.userService.checkAndSetUrl(form.value.url).pipe(
          map(() => checkUrlSuccess()),
          catchError(() => of(checkUrlFailure()))
        );
      })
    )
  );

  register$ = createEffect(() =>
    this.actions$.pipe(
      ofType(register),
      withLatestFrom(this.store.pipe(select(fromAccount.getRegisterForm))),
      map(([action, form]) => form.value),
      exhaustMap(auth =>
        this.userService
          .register(
            auth.email,
            auth.password,
            auth.rememberMe ? auth.rememberMe : false
          )
          .pipe(
            map(resp => registerSuccess({ payload: resp })),
            catchError(err => of(registerFailure(err)))
          )
      )
    )
  );

  constructor(
    private actions$: Actions,
    private userService: UserService,
    private store: Store<IState>
  ) {}
}
