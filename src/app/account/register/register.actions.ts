import { createAction, props } from "@ngrx/store";
import { IRegisterSuccessResponse } from "./interfaces";

export const register = createAction("[Register] Register");
export const registerSuccess = createAction(
  "[Register] Success",
  props<{ payload: IRegisterSuccessResponse }>()
);
export const registerFailure = createAction(
  "[Register] Failure",
  props<{ payload: string }>()
);
export const registerClear = createAction("[Register] Clear");
export const checkEmail = createAction("[Register] Check Username");
export const checkEmailSuccess = createAction(
  "[Register] Check Username Success"
);
export const checkEmailFailure = createAction(
  "[Register] Check Username Failure",
  props<{ payload: string }>()
);
export const setIsEmailTaken = createAction(
  "[Register] Set Email Is Taken",
  props<{ payload: boolean }>()
);
export const checkUrl = createAction("[Register] Check Url");
export const checkUrlSuccess = createAction("Check Url Success");
export const checkUrlFailure = createAction("[Register] Check Url Failure");
export const switchStage = createAction(
  "Switch Stage",
  props<{ payload: number }>()
);
export const incrementStage = createAction("Increment Stage");
export const displayUrl = createAction("Display Url");
export const hideUrl = createAction("Hide Url");
