import { createAction, props } from "@ngrx/store";

export const verifyEmail = createAction(
  "[Confirm email] Veify Email",
  props<{ payload: string }>()
);
export const verifyEmailSuccess = createAction(
  "[Confirm email] Veify Email Success"
);
export const verifyEmailFailure = createAction(
  "[Confirm email] Veify Email Failure",
  props<{ payload: string }>()
);
export const resetRegisterCode = createAction(
  "[Confirm email] ResetRegisterCode"
);
export const resetRegisterCodeSuccess = createAction("[Confirm email] Success");
export const resetRegisterCodeFailure = createAction("[Confirm email] Failure");
