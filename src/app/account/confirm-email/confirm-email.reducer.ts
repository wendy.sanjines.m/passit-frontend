import { createReducer, on, Action } from "@ngrx/store";
import {
  verifyEmail,
  verifyEmailSuccess,
  verifyEmailFailure,
  resetRegisterCode,
  resetRegisterCodeSuccess,
  resetRegisterCodeFailure
} from "./confirm-email.actions";

export interface IConfirmEmailState {
  resetCodeMessage: string | null;
  isVerified: boolean;
  hasStarted: boolean;
  hasFinished: boolean;
  errorMessage: string | null;
}

export const initialState: IConfirmEmailState = {
  resetCodeMessage: null,
  isVerified: false,
  hasStarted: false,
  hasFinished: false,
  errorMessage: null
};

export const confirmEmailReducer = createReducer(
  initialState,
  on(verifyEmail, state => ({
    ...state,
    hasStarted: true,
    errorMessage: null
  })),
  on(verifyEmailSuccess, state => ({
    ...state,
    hasStarted: false,
    hasFinished: true,
    errorMessage: null,
    isVerified: true
  })),
  on(verifyEmailFailure, (state, action) => ({
    ...state,
    hasStarted: false,
    hasFinished: false,
    errorMessage: action.payload
  })),
  on(resetRegisterCode, state => ({ ...state, resetCodeMessage: null })),
  on(resetRegisterCodeSuccess, state => ({
    ...state,
    resetCodeMessage: "Check your email for a new confirmation code."
  })),
  on(resetRegisterCodeFailure, state => ({
    ...state,
    resetCodeMessage: "Unable to resend confirmation code."
  }))
);

export function reducer(state: IConfirmEmailState, action: Action) {
  return confirmEmailReducer(state, action);
}

export const getResetCodeMessage = (state: IConfirmEmailState) =>
  state.resetCodeMessage;
export const getIsVerified = (state: IConfirmEmailState) => state.isVerified;
export const getHasStarted = (state: IConfirmEmailState) => state.hasStarted;
export const getHasFinished = (state: IConfirmEmailState) => state.hasFinished;
export const getErrorMessage = (state: IConfirmEmailState) =>
  state.errorMessage;
