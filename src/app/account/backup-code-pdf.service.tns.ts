import { Injectable } from "@angular/core";

@Injectable({
  providedIn: "root"
})
export class BackupCodePdfService {
  constructor() {}

  /**
   * Downloads PDF containing backup code string and matching QR Code
   * @param code: 32 character backupcode string
   */
  download(code: string) {}
}
