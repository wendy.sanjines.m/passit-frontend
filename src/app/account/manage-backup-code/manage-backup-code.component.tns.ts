import {
  Component,
  Input,
  Output,
  ChangeDetectionStrategy,
  EventEmitter,
  OnDestroy
} from "@angular/core";
import { FormGroupState, MarkAsSubmittedAction } from "ngrx-forms";
import { IForm } from "./manage-backup-code.reducer";
import { Router } from "@angular/router";
import { ActionsSubject } from "@ngrx/store";

@Component({
  selector: "app-manage-backup-code",
  templateUrl: "./manage-backup-code.component.html",
  styleUrls: [
    "./manage-backup-code.component.scss",
    "../account.component.scss",
    "../../../styles/_utility.scss"
  ],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ManageBackupCodeComponent implements OnDestroy {
  @Input() form: FormGroupState<IForm>;
  @Input() hasStarted: boolean;
  @Input() hasFinished: boolean;
  @Input() nonFieldErrors: string[];
  @Input() backupCode: string;

  @Output() newBackupCode = new EventEmitter();
  @Output() reset = new EventEmitter();

  onSubmit() {
    if (this.form.isValid) {
      this.newBackupCode.emit();
    } else if (this.form.isUnsubmitted) {
      this.actionsSubject.next(new MarkAsSubmittedAction(this.form.id));
    }
  }

  ngOnDestroy() {
    this.reset.emit();
  }

  learnMore() {
    this.router.navigate(["/account/forgot-password"]);
  }

  constructor(private router: Router, private actionsSubject: ActionsSubject) {}
}
