import { Component, OnInit } from "@angular/core";
import * as fromRoot from "../../app.reducers";
import * as fromAccount from "../account.reducer";
import { ResetDeleteState, DeleteAccount, ResetErrorMessage } from "../delete/delete.actions";
import { Store, select } from "@ngrx/store";

@Component({
  selector: "app-delete-container",
  template: `
    <app-delete
      [form]="form$ | async"
      [errorMessage]="errorMessage$ | async"
      [hasStarted]="hasStarted$ | async"
      (doAccountDelete)="doAccountDelete()"
      (resetErrorMessage)="resetErrorMessage()"
    ></app-delete>
  `
})
export class DeleteContainer implements OnInit {
  form$ = this.store.pipe(select(fromAccount.getDeleteForm));
  errorMessage$ = this.store.pipe(select(fromAccount.getDeleteErrorMessage));
  hasStarted$ = this.store.pipe(select(fromAccount.getDeleteHasStarted));
  constructor(private store: Store<fromRoot.IState>) {}

  ngOnInit() {
    this.store.dispatch(new ResetDeleteState());
  }

  doAccountDelete() {
    this.store.dispatch(new DeleteAccount());
  }

  resetErrorMessage() {
    this.store.dispatch(new ResetErrorMessage());
  }
}
