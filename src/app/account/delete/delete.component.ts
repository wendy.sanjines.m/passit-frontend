import { Component, EventEmitter, Output, Input } from "@angular/core";
import { IDeleteAccountForm } from "./delete.reducer";
import { FormGroupState } from "ngrx-forms";

@Component({
  selector: "app-delete",
  templateUrl: "./delete.component.html",
  styleUrls: ["./delete.component.scss"]
})
export class DeleteComponent {
  @Input() form: FormGroupState<IDeleteAccountForm>;
  @Input() errorMessage: string;
  @Input() hasStarted: boolean;
  @Output() doAccountDelete = new EventEmitter<string>();
  @Output() resetErrorMessage = new EventEmitter();
  showConfirm = true;

  toggleConfirm() {
    this.showConfirm = !this.showConfirm;
  }
}
