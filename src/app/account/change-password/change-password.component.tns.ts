import { Component, EventEmitter, Input, Output } from "@angular/core";
import { FormGroupState, MarkAsSubmittedAction } from "ngrx-forms";
import { ActionsSubject } from "@ngrx/store";
import { Router } from "@angular/router";

import { IChangePasswordForm } from "../change-password/change-password.reducer";

@Component({
  selector: "change-password",
  moduleId: module.id,
  templateUrl: "./change-password.component.html"
})
export class ChangePasswordComponent {
  @Input() form: FormGroupState<IChangePasswordForm>;
  @Input() nonFieldErrors: string[];
  @Input() hasStarted: boolean;
  @Input() hasFinished: boolean;
  @Input() backupCode: string;

  @Output() changePassword = new EventEmitter();
  @Output() toggleConfirm = new EventEmitter();

  constructor(private actionsSubject: ActionsSubject, private router: Router) {}

  onSubmit() {
    if (this.form.isValid) {
      this.changePassword.emit();
    } else if (this.form.isUnsubmitted) {
      this.actionsSubject.next(new MarkAsSubmittedAction(this.form.id));
    }
  }

  changeBackupCode() {
    this.router.navigate(["/account/change-backup-code"]);
  }
}
