import { Component, EventEmitter, Input, Output } from "@angular/core";
import * as dialogs from "tns-core-modules/ui/dialogs";
import { Router } from "@angular/router";

@Component({
  selector: "forgot-learn-more",
  templateUrl: "./forgot-learn-more.component.html",
  styleUrls: ["./forgot-learn-more.component.css"]
})
export class ForgotLearnMoreComponent {
  @Input() confirmText: string;

  @Output() logOut = new EventEmitter();

  onLogOut() {
    dialogs
      .confirm({
        message: this.confirmText,
        okButtonText: "Log Out",
        cancelButtonText: "Cancel"
      })
      .then(result => result && this.logOut.emit());
  }

  importPasswords() {
    this.router.navigate(["../import"]);
  }

  exportPasswords() {
    this.router.navigate(["../export"]);
  }

  constructor(private router: Router) {}
}
