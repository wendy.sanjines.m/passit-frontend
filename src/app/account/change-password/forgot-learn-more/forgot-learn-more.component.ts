import { Component, EventEmitter, Input, Output } from "@angular/core";

@Component({
  selector: "forgot-learn-more",
  templateUrl: "./forgot-learn-more.component.html",
  styleUrls: [
    "./forgot-learn-more.component.css",
    "../../account.component.scss"
  ]
})
export class ForgotLearnMoreComponent {
  @Input() confirmText: string;

  @Output() logOut = new EventEmitter();

  onLogOut() {
    if (window.confirm(this.confirmText)) {
      this.logOut.emit();
    }
  }
}
