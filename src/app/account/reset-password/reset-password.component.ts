import {
  Component,
  ChangeDetectionStrategy,
  Input,
  Output,
  EventEmitter,
  ViewChild,
  ElementRef,
  OnInit
} from "@angular/core";

import { FormGroupState } from "ngrx-forms";

import { IResetPasswordForm } from "./reset-password.reducer";

@Component({
  selector: "app-reset-password",
  templateUrl: "./reset-password.component.html",
  styleUrls: ["../account.component.scss", "./reset-password.styles.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ResetPasswordComponent implements OnInit {
  @Input() isExtension: boolean;
  @Input() form: FormGroupState<IResetPasswordForm>;
  @Input() errorMessage: string;
  @Input() hasStarted: boolean;
  @Input() hasFinished: boolean;
  @Input() emailDisplayName: string;

  @Output() submitEmail = new EventEmitter();
  @Output() reset = new EventEmitter();

  @ViewChild("emailInput", { static: false })
  emailInput: ElementRef;

  constructor() {}

  ngOnInit() {
    if (this.emailInput) {
      this.emailInput.nativeElement.focus();
    }
  }

  onSubmit() {
    if (this.form.isValid) {
      this.submitEmail.emit();
      if (this.emailInput) {
        this.emailInput.nativeElement.blur();
      }
    }
  }

  onReset() {
    this.reset.emit();
    setTimeout(() => this.emailInput.nativeElement.focus(), 0);
  }
}
