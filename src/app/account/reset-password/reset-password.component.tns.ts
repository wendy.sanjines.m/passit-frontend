import {
  Component,
  ChangeDetectionStrategy,
  Input,
  Output,
  EventEmitter,
  OnInit
} from "@angular/core";
import { RouterExtensions } from "nativescript-angular/router";
import { FormGroupState, MarkAsSubmittedAction } from "ngrx-forms";
import { ActionsSubject } from "@ngrx/store";

import { IResetPasswordForm } from "./reset-password.reducer";

@Component({
  selector: "app-reset-password",
  templateUrl: "./reset-password.component.html",
  styleUrls: ["../account.component.tns.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ResetPasswordComponent implements OnInit {
  @Input() isExtension: boolean;
  @Input() form: FormGroupState<IResetPasswordForm>;
  @Input() errorMessage: string;
  @Input() hasStarted: boolean;
  @Input() hasFinished: boolean;
  @Input() emailDisplayName: string;

  @Output() submitEmail = new EventEmitter();
  @Output() reset = new EventEmitter();

  public htmlString: string;
  constructor(
    private routerExtensions: RouterExtensions,
    private actionsSubject: ActionsSubject
  ) {}
  ngOnInit() {}

  onSubmit() {
    if (this.form.isValid) {
      this.submitEmail.emit();
    } else if (this.form.isUnsubmitted) {
      this.actionsSubject.next(new MarkAsSubmittedAction(this.form.id));
    }
  }

  onResendEmail() {
    this.submitEmail.emit();
  }

  onReset() {
    this.reset.emit();
  }

  goToLogin() {
    this.routerExtensions.navigate(["/account/login"], { clearHistory: true });
  }
}
