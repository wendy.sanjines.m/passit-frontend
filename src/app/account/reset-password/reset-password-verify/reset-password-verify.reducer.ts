import {
  validate,
  updateGroup,
  createFormGroupState,
  FormGroupState,
  createFormStateReducerWithUpdate,
  onNgrxForms,
  wrapReducerWithFormStateUpdate
} from "ngrx-forms";
import { minLength } from "ngrx-forms/validation";
import { createReducer, on, Action } from "@ngrx/store";
import { IBaseFormState } from "../../../utils/interfaces";
import * as ResetPasswordVerifyActions from "./reset-password-verify.actions";

const FORM_ID = "Reset Password Form";

export interface IResetPasswordVerifyForm {
  code: string;
}

const validateAndUpdateResetPasswordVerifyFormState = updateGroup<
  IResetPasswordVerifyForm
>({
  code: validate(minLength(32))
});

export const initialResetPasswordVerifyFormState = validateAndUpdateResetPasswordVerifyFormState(
  createFormGroupState<IResetPasswordVerifyForm>(FORM_ID, {
    code: ""
  })
);

export interface IResetPasswordVerifyState extends IBaseFormState {
  form: FormGroupState<IResetPasswordVerifyForm>;
  errorMessage: string | null;
  email: string | null;
  emailCode: string | null;
}

export const initialState: IResetPasswordVerifyState = {
  form: initialResetPasswordVerifyFormState,
  hasStarted: false,
  hasFinished: false,
  errorMessage: null,
  email: null,
  emailCode: null
};

export const formReducer = createFormStateReducerWithUpdate<
  IResetPasswordVerifyForm
>(validateAndUpdateResetPasswordVerifyFormState);

export const resetPasswordVerifyReducer = createReducer(
  initialState,
  onNgrxForms(),
  on(ResetPasswordVerifyActions.init, (state, action) => {
    if (action.email && action.code) {
      return {
        ...state,
        email: action.email,
        emailCode: action.code,
        errorMessage: null
      };
    } else {
      return {
        ...state,
        errorMessage: "Invalid reset password link"
      };
    }
  }),
  on(ResetPasswordVerifyActions.verifyAndLogin, state => ({
    ...state,
    hasStarted: true
  })),
  on(ResetPasswordVerifyActions.verifyAndLoginFailure, state => ({
    ...state,
    hasStarted: false,
    errorMessage:
      "Unable to validate backup code. Verify that this is your most recent code and that it was entered correctly."
  }))
);

const wrappedReducer = wrapReducerWithFormStateUpdate(
  resetPasswordVerifyReducer,
  s => s.form,
  validateAndUpdateResetPasswordVerifyFormState
);

export function reducer(
  state: IResetPasswordVerifyState | undefined,
  action: Action
) {
  return wrappedReducer(state, action);
}

export const getForm = (state: IResetPasswordVerifyState) => state.form;
export const getHasStarted = (state: IResetPasswordVerifyState) =>
  state.hasStarted;
export const getErrorMessage = (state: IResetPasswordVerifyState) =>
  state.errorMessage;
export const getEmailAndCode = (state: IResetPasswordVerifyState) => {
  return { email: state.email, emailCode: state.emailCode };
};
