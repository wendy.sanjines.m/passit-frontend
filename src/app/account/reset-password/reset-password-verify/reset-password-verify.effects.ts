import { Injectable } from "@angular/core";
import { Router } from "@angular/router";
import { createEffect, Actions, ofType } from "@ngrx/effects";
import { Store, select } from "@ngrx/store";
import { of } from "rxjs";
import { withLatestFrom, map, mergeMap, tap, catchError } from "rxjs/operators";
import { IState } from "../../../app.reducers";
import { getResetPasswordVerifyEmailAndCode } from "../../account.reducer";
import { UserService } from "../../user";
import * as ResetPasswordVerifyActions from "./reset-password-verify.actions";

@Injectable()
export class ResetPasswordVerifyEffects {
  verifyAndLogin$ = createEffect(() =>
    this.actions$.pipe(
      ofType(ResetPasswordVerifyActions.verifyAndLogin),
      withLatestFrom(
        this.store.pipe(select(getResetPasswordVerifyEmailAndCode))
      ),
      map(([action, emailAndCode]) => [
        action.payload,
        emailAndCode.email,
        emailAndCode.emailCode
      ]),
      mergeMap(([backupCode, email, emailCode]) => {
        if (email && emailCode) {
          return this.userService.resetPasswordVerify(email, emailCode).pipe(
            mergeMap(resp =>
              this.userService.resetPasswordVerifyLogin(
                email,
                emailCode,
                resp.private_key_backup,
                backupCode!,
                resp.server_public_key
              )
            ),
            map(auth => ResetPasswordVerifyActions.verifyAndLoginSuccess(auth)),
            catchError(err =>
              of(
                ResetPasswordVerifyActions.verifyAndLoginFailure({
                  payload: err
                })
              )
            )
          );
        }
        return of(
          ResetPasswordVerifyActions.verifyAndLoginFailure({
            payload: "Invalid recovery link"
          })
        );
      })
    )
  );

  verifyAndLoginSuccess$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(ResetPasswordVerifyActions.verifyAndLoginSuccess),
        tap(() => {
          this.router.navigate(["account/reset-password/set-password"]);
        })
      ),
    { dispatch: false }
  );

  constructor(
    private actions$: Actions,
    private store: Store<IState>,
    private router: Router,
    private userService: UserService
  ) {}
}
