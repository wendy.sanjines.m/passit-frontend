import { Action } from "@ngrx/store";

export enum ResetPasswordActionTypes {
  SUBMIT_FORM = "[Reset Password] Submit",
  SUBMIT_FORM_SUCCESS = "[Reset Password] Submit Success",
  SUBMIT_FORM_FAILURE = "[Reset Password] Submit Failure",
  RESET_FORM = "[Reset Password] Reset"
}

export class SubmitForm implements Action {
  readonly type = ResetPasswordActionTypes.SUBMIT_FORM;
}

export class SubmitFormSuccess implements Action {
  readonly type = ResetPasswordActionTypes.SUBMIT_FORM_SUCCESS;
}

export class SubmitFormFailure implements Action {
  readonly type = ResetPasswordActionTypes.SUBMIT_FORM_FAILURE;
}

export class ResetForm implements Action {
  readonly type = ResetPasswordActionTypes.RESET_FORM;
}

export type ResetPasswordActionsUnion =
  | SubmitForm
  | SubmitFormSuccess
  | SubmitFormFailure
  | ResetForm;
