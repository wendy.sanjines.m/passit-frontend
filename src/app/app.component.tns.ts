import { Component, OnInit } from "@angular/core";
import { registerElement } from "nativescript-angular/element-registry";
import { Store, select } from "@ngrx/store";

import { AppDataService } from "./shared/app-data/app-data.service";
import { GetConfAction } from "./get-conf/conf.actions";
import { Sentry } from "nativescript-sentry";
import { getRavenDsn } from "./app.reducers";
import { filter } from "rxjs/operators";

registerElement("Fab", () => require("nativescript-floatingactionbutton").Fab);

@Component({
  selector: "app-root",
  moduleId: module.id,
  templateUrl: "./app.component.html"
})
export class AppComponent implements OnInit {
  constructor(
    private appDataService: AppDataService,
    private store: Store<any>
  ) {
    this.appDataService.rehydrate();
  }

  ngOnInit() {
    this.store.dispatch(new GetConfAction());
    this.store
      .pipe(
        select(getRavenDsn),
        filter(dsn => dsn !== null)
      )
      .subscribe((dsn: string) => {
        Sentry.init(dsn);
      });
  }
}
