apiVersion: apps/v1
kind: Deployment
metadata:
  name: {{ include "passit.fullname" . }}
  labels:
    {{- include "passit.labels" . | nindent 4 }}
spec:
  replicas: {{ .Values.web.replicaCount }}
  selector:
    matchLabels:
      {{- include "passit.selectorLabels" . | nindent 6 }}
      role: web
  template:
    metadata:
      annotations:
        checksum/secret: {{ include (print $.Template.BasePath "/secrets.yaml") . | sha256sum }}
        checksum/configmap: {{ include (print $.Template.BasePath "/configmap.yaml") . | sha256sum }}
        tag: {{ .Values.image.tag }}
      labels:
        {{- include "passit.selectorLabels" . | nindent 8 }}
        role: web
    spec:
    {{- with .Values.imagePullSecrets }}
      imagePullSecrets:
        {{- toYaml . | nindent 8 }}
    {{- end }}
      serviceAccountName: {{ include "passit.serviceAccountName" . }}
      securityContext:
        {{- toYaml .Values.podSecurityContext | nindent 8 }}
      containers:
        - name: {{ .Chart.Name }}
          securityContext:
            {{- toYaml .Values.securityContext | nindent 12 }}
          image: "{{ .Values.image.repository }}:{{ .Values.image.tag }}"
          imagePullPolicy: {{ .Values.image.pullPolicy }}
          ports:
            - name: http
              containerPort: 8080
              protocol: TCP
          livenessProbe:
            failureThreshold: {{ .Values.web.livenessProbe.failureThreshold }}
            httpGet:
              path: /_health/
              port: 8080
            initialDelaySeconds: {{ .Values.web.livenessProbe.initialDelaySeconds }}
            timeoutSeconds: {{ .Values.web.livenessProbe.timeoutSeconds }}
          readinessProbe:
            failureThreshold: {{ .Values.web.readinessProbe.failureThreshold }}
            httpGet:
              path: /_health/
              port: 8080
            initialDelaySeconds: {{ .Values.web.readinessProbe.initialDelaySeconds }}
            timeoutSeconds: {{ .Values.web.readinessProbe.timeoutSeconds }}
          resources:
            {{- toYaml .Values.web.resources | nindent 12 }}
          env:
            - name: DEBUG
              value: "False"
            {{- if .Values.redisURL }}
            - name: REDIS_URL
              value: {{ .Values.redisURL }}
            {{- end }}
            {{- if .Values.redis.enabled }}
            - name: REDIS_PASSWORD
              valueFrom:
                secretKeyRef:
                  key: redis-password
                  name: {{ include "passit.fullname" . }}-redis
            - name: REDIS_HOST
              value: {{ template "passit.redis.host" . }}
            - name: REDIS_PORT
              value: {{ template "passit.redis.port" . }}
            {{- end }}
          envFrom:
            - secretRef:
                name: {{ include "passit.fullname" . }}
            - configMapRef:
                name: {{ include "passit.fullname" . }}
      {{- with .Values.web.nodeSelector }}
      nodeSelector:
        {{- toYaml . | nindent 8 }}
      {{- end }}
    {{- with .Values.web.affinity }}
      affinity:
        {{- toYaml . | nindent 8 }}
    {{- end }}
    {{- with .Values.web.tolerations }}
      tolerations:
        {{- toYaml . | nindent 8 }}
    {{- end }}
